const TelegramBot = require('node-telegram-bot-api')
const nanoid = require('nanoid')
const knex = require('knex')(require('./knexfile'))

const token = process.env.TELEGRAM_BOT_TOKEN

const bot = new TelegramBot(token, { polling: true })

bot.on('callback_query', async ({ id, data }) => {
  try {
    const { content } = await knex('cw').where({ id: data }).first('content')

    bot.answerCallbackQuery(id, {
      show_alert: true,
      text: content,
    })
  } catch (e) {
    console.log('Got error', e)

    bot.answerCallbackQuery(id, {
      show_alert: true,
      text: 'ERROR: no se pudo encontrar el mensaje :(',
    })
  }
})

bot.on('inline_query', async ({ id, query }) => {
  let [cw, ...text] = query.split('\n\n')
  text = text.join('\n\n')

  const match = query.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/)
  let url
  let urlCw
  if (match) {
    url = match[0]
    urlCw = cw.slice(0, match.index)
  }

  const invalidText = (query.length < 1 || cw.length < 1 || text.length < 1)
  if (invalidText && !url) {
    bot.answerInlineQuery(id, [{
      type: 'article',
      id: nanoid(),
      title: `Tenés que especificar un CW...`,
      description: 'El formato es: [content warning], dos "enter" y [el texto], o también puede ser [content warning] y [url]',
      message_text: 'cw invalido',
    }])
    return
  }

  const cwId = (await knex('cw').insert({
    content: text,
  }))[0]
  const emoji = ['⚠️', '🔓']

  bot.answerInlineQuery(id, [...(!invalidText ? [{
    type: 'article',
    id: nanoid(),
    title: `Crear mensaje con el CW ${cw}`,
    description: `y el texto ${text}`,
    input_message_content: {
      parse_mode: 'html',
      message_text: `${emoji[0]} Content Warning: ${cw}`,
    },
    reply_markup: {
      inline_keyboard: [[{
        text: `${emoji[1]} Ver contenido`,
        callback_data: cwId,
      }]],
    },
  }] : []), ...(url ? [{
    type: 'article',
    id: nanoid(),
    title: `Mandar link con el CW ${urlCw}`,
    description: `url: ${url}`,
    input_message_content: {
      parse_mode: 'html',
      message_text: `${emoji[0]} Link con Content Warning: ${urlCw}`,
    },
    reply_markup: {
      inline_keyboard: [[{
        text: `${emoji[1]} Ver contenido`,
        url,
      }]],
    },
  }] : [])])
})
